function mru(){
  var nu1=parseInt(document.getElementById('distanciamru').value);
  var nu2=parseInt(document.getElementById('tiempomru').value);
  var res;
  res=nu1/nu2;
  var res2= res.toFixed(3)
  alertify.alert("VELOCIDAD","tu velocidad fue " +res2 +"m/s").set('label', 'Aceptar')
  var confirm=alertify.alert("","",null,null).set('labels',{ok: 'listo'})
  confirm.set('onok', function(){
    alertify.success('Vamos a probar lo siguiente, esto fue muy facil!!')
  });
}
function tirovertical(){
  var vo=parseInt(document.getElementById('votv').value);
  var t=parseInt(document.getElementById('ttv').value);
  var res;
  res=(vo*t)+((-9.82)*(t^2))/2;
  var tt=t*2;
  alertify.alert("ALTURA ALCANZADA Y MAS","Con velocidad inicial de " +vo +"m/s y un tiempo de " +t +" segundos el proyectil subio " +res +" metros, tardara en caer " +tt +" segundos y regresara con una velocidad de " +vo +"m/s ya que salio con esa velocidad, regresara con la misma.").set('label', 'Aceptar')
  var confirm=alertify.alert("","",null,null).set('labels',{ok: 'listo'})
  confirm.set('onok', function(){
    alertify.success('Trata con caída libre!!')
  });
}
function cl(){
  var y=parseInt(document.getElementById('alturacl').value);
  var res;
  res= Math.sqrt((2*y)/9.81);
  var res2= res.toFixed(3)
  var v2=9.81*res2
  var v22= v2.toFixed(3)
  alertify.alert("VELOCIDAD FINAL","el proyectil llegara al suelo en " +res2 +" segundos y llegara con una velocidad de " +v22 +"m/s").set('label', 'Aceptar')
  var confirm=alertify.alert("","",null,null).set('labels',{ok: 'listo'})
  confirm.set('onok', function(){
    alertify.success('Vamos a lo mejor!!')
  });
}
function tp(){
  var tiempo=parseInt(document.getElementById('ttp').value);
  var vo=parseInt(document.getElementById('votp').value);
  var angulo=parseInt(document.getElementById('angulotp').value);
  var vox= vo*(Math.cos(angulo))
  var voxx=Math.abs(vox.toFixed(3))
  var voy= vo*(Math.sin(angulo))
  var voyy=Math.abs(voy.toFixed(3))
  var d2=voxx*tiempo
  var d=Math.abs(d2.toFixed(4))
  var y2=(voyy*tiempo)+((9.81*(tiempo*tiempo))/2)
  var y=Math.abs(y2.toFixed(4))
  alertify.alert("Resultado final","La distancia horizontalmente que alcanzo tu proyectil fueron " +d +"m, llego una altura máxima de " +y +"m sobre la horizontal y aterrizo en el suelo con una velocidad en x=" +voxx +"m/s y en y=" +voyy +"m/s que al ser sumadas nos devuelve la velocidad final total que serian " +vo +"m/s").set('label', 'Aceptar')
  var confirm=alertify.alert("","",null,null).set('labels',{ok: 'listo'})
  confirm.set('onok', function(){
    alertify.success('ESPERO QUE ESTOS EJEMPLOS TE HAYAN SERVIDO PARA ENTENDERLO MEJOR')
  });
}
